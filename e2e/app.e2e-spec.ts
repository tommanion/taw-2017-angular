import { Taw2017AngularPage } from './app.po';

describe('taw-2017-angular App', function() {
  let page: Taw2017AngularPage;

  beforeEach(() => {
    page = new Taw2017AngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
